"""Parses randomized diagonal matrices per chromosome and returns 10*10 submatrices"""

import pandas as pd
import numpy as np

def parseRandomDiagFile(randomDiagFileName):
    randomDiags = pd.read_table(randomDiagFileName, header = None)
    return randomDiags

def getRandomSubMatrices(randomDiagFileName, nBins = 10):
    randomDiags = parseRandomDiagFile(randomDiagFileName)
    randomDiags = randomDiags.drop([10], axis=1)
    dim = randomDiags.shape
    randomDiags_split = np.array_split(randomDiags, np.around(dim[0]/nBins))
    
    return randomDiags_split
    
if __name__ == "__main__":
    
    random_diag_file = "E:/MJT_hackathon/cmatr/hES/nij/randomized_diagonals/rdiag.chr20"
    test = getRandomSubMatrices(random_diag_file)
    print(test[0])