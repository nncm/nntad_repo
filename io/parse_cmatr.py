"""Parser script for the TAD NN
Reads a contact matrix and returns it without the lower diagonal"""
# from __future__ import division
import pandas as pd
import numpy as np
import scipy.misc

def returnDiagonal(fileName):
	"""
	Parses Hi-C contact matrix from text and returns upper diagonal (including diagonal). 
	The rest of the elements are NaN.
	Sij !=NaN for i<=j
	The NaN behaviour may be a problem later with scikit learn. If so, set lower diagonal to 0
	"""
	inFile = pd.read_table(fileName, header = None)
	dim = inFile.shape
	mask = np.zeros(inFile.shape, dtype = 'bool')
	mask[np.triu_indices(dim[0], 0)] = True
	inFile = inFile[(inFile >= 0) &  mask]
	inFile =  inFile.fillna(value=-1) # to substitute NaNs
	return inFile

def parseTadCoordinates(tadFileName):
	"""
	Opens a bed file and parses TAD coordinates from it
	"""
	tads = pd.read_table(tadFileName, header = None, names = ["chr", "start", "end"])
	return tads 

def returnTadTrainMatr(tadFileName, chromosome,fraction = 1, tadPos = True,step =1000, reshaping_factor = 63,bin_size=40000):
	"""
	Returns a contact matrix subset that contains a TAD but also includes contact matrix 
	regions beyond the TAD interval.
	@param
	tadFileName (string) - name/path of the file to a bed file with TAD coordinates
	fraction (int) - determines the size of the region that the NN will explore
	tadPos (bool) - determines the relative position of the TAD boundaries within the returned matrix.
	@return
	The returned matrix width is fraction*TADlength
	If tadPos == True the downstream TAD boundary is located at the center of the coordinates
	of the returned contact matrix, otherwise the position is random
	"""
	tadCoords = parseTadCoordinates(tadFileName)
	reshaping_factor = reshaping_factor*bin_size
	st_subset = []
	end_subset = []
	dif_subset = []
	tadCoords = tadCoords[tadCoords["chr"]=="chr{0}".format(chromosome)] # we only have data from ch4 in the matrix
	o = 0
	if tadPos:
		for chrom, start, end in tadCoords.values:
			subset_length = abs(start-end)/fraction
			# depending on position we will have to add or subtract the subset_length
			if end > start:
				st_subset += [start - subset_length]
				end_subset += [end + subset_length]
			else:
				st_subset += [start + subset_length]
				end_subset += [end - subset_length]
			diff = abs(st_subset[-1]-end_subset[-1])
			# this part is for reshaping, reshaping_factor is the size
			# that our final matrix is going to be
			diff, st_subset, end_subset = reshaper(reshaping_factor, diff, 
													start, end, st_subset, end_subset, step)
			dif_subset += [diff]
	else:
		print "Go for random" 
		for chrom, start, end in tadCoords.values:
			subset_length = abs(start-end)/fraction
			if end > start:
				old_start = start-subset_length
				st_subset += [np.random.randint(low=old_start,high=start)]
				end_subset += [(end + subset_length)+(st_subset[-1] - old_start) ]
			else:
				old_end = end-subset_length
				end_subset += [np.random.randint(low=old_end,high=end)]
				st_subset += [(start + subset_length)+(end_subset[-1] - old_end) ]
			diff = abs(st_subset[-1]-end_subset[-1])
			# diff, st_subset, end_subset = reshaper(reshaping_factor, diff, 
													# start, end, st_subset, end_subset, step)
			if reshaping_factor < diff:
				print "*****"
				while reshaping_factor < diff:
					if end > start:

						# print end_subset[-1]
						end_subset[-1] = end_subset[-1]-step
					else:

						st_subset[-1] = st_subset[-1]-step
					diff = abs(st_subset[-1]-end_subset[-1])	
					# print diff	
				print reshaping_factor > diff, end_subset[-1], st_subset[-1], diff,diff-reshaping_factor
			else:
				print "---"
				while reshaping_factor > diff:
					# print 'he'
					if end > start:
						# print end_subset[-1]
						end_subset[-1] = end_subset[-1]+step
					else:
						st_subset[-1] = st_subset[-1]+step
					diff = abs(st_subset[-1]-end_subset[-1])
					# print diff
				print reshaping_factor < diff, end_subset[-1], st_subset[-1],diff, diff-reshaping_factor
			dif_subset += [diff]

	tadCoords = tadCoords.assign(start_subset=st_subset, end_subset=end_subset, dif=dif_subset)	
	tadCoords
	
	return tadCoords


def reshaper(reshaping_factor, diff, start, end, st_subset, end_subset, step):
	if reshaping_factor < diff:
		while reshaping_factor < diff:
			if end > start:
				end_subset[-1] = end_subset[-1]-step
			else:
				st_subset[-1] = st_subset[-1]-step
			diff = abs(st_subset[-1]-end_subset[-1])
	else:
		while reshaping_factor > diff:
			if end > start:
				end_subset[-1] = end_subset[-1]+step
			else:
				st_subset[-1] = st_subset[-1]+step
			diff = abs(st_subset[-1]-end_subset[-1])
	# print st_subset[-1]-end_subset[-1]
	return diff, st_subset, end_subset


# def reshape_matrix(matrix_file, tad_coords, bin_size=40000):
# 	"""
# 	Not working
# 	"""
# 	hicMatr = returnDiagonal(matrix_file).as_matrix()
# 	final_subsets = []
# 	tad_coords = tad_coords[["start_subset", "end_subset"]]
# 	for x, y in tad_coords.values:
# 		# print x, y
# 		x = x/bin_size
# 		y = y/bin_size
# 		candidate = hicMatr[x:y+1, x:y+1]
# 		if len(candidate) != 0:
# 			print candidate[0].shape
# 			final_subsets += scipy.misc.imresize(candidate[0], (64,64), interp="bicubic").astype(np.float64, casting='unsafe')
# 			# final_subsets += [hicMatr[x:y+1, x:y+1]]
# 	lengths = [len(i) for i in final_subsets]
# 	print lengths
# 	print "Mean=", np.mean(lengths), "Median:", np.median(lengths)
# 		# print x,y+1, y+1-x, final_subsets[-1].shape
# 	# print len(hicMatr)
# 	# for tad in final_subsets:
# 	# 	print len(tad)
# 		# exit()
	

def subset_matrix(matrix_file, tad_coords, bin_size=40000):
	"""
	Subsets the contact matrix according to TADS coordinates.
	"""
	hicMatr = returnDiagonal(matrix_file).as_matrix()

	final_subsets = []
	tad_coords = tad_coords[["start_subset", "end_subset"]]
	for x, y in tad_coords.values:
		# print x, y
		x = x/bin_size
		y = y/bin_size
		candidate = hicMatr[x:y+1, x:y+1]
		if len(candidate) == 64:
			final_subsets += [hicMatr[x:y+1, x:y+1]]
			# print x,y+1, x-y+1, hicMatr[x:y+1, x:y+1].shape
	lengths = [len(i) for i in final_subsets]
	final_subsets = np.asarray(final_subsets)
	# print lengths
	# print len(final_subsets[-1])
	# print "Mean=", np.mean(lengths), "Median:", np.median(lengths)

	# print type(final_subsets), type(final_subsets[0]), type(final_subsets[0][0]), type(final_subsets[0][0][0])
	return final_subsets

	
if __name__ == "__main__":
	tad_domains = "/home/participant/Desktop/Group_Share/nntad/hicdata/mESC/HindIII_combined/total.HindIII.combined.domain"
	matrix_file = "/home/participant/Desktop/Group_Share/nntad/hicdata/mES_bed/nij/nij.chr9"
	chromosome = 4
	tadCoords = returnTadTrainMatr(tad_domains, chromosome)
	subset_matrix(matrix_file, tadCoords)
