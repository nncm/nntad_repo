"""Parses a Hi-C contact matrix and a bed file containing TAD boundary locations. Returns a dictionary of matrices that contain Hi-C contact sub-matrices at the TAD boundaries. All sub-matrices are square and are of the same dimensions, the size of which is settable."""

import pandas as pd
import numpy as np

def parseTadCoordinates(tadFileName):
    """
    Opens a bed file, parses TAD start and end coordinates from it and returns as a pandas object with three columns
    """
    tads = pd.read_table(tadFileName, header = None, names = ["chr", "start", "end"])
    return tads

def parseTadCoordinates_boundariesOnly(tadFileName):
    """Parses TAD boundary coordinates file and returns a pandas object with two columns"""
    tads = pd.read_table(tadFileName, header = None, names = ["chr", "boundary"])
    return tads

def getBoundarySubMatrices_oneChr(hicFileName, tadCoords, subMatrSize):
    """
    Opens a Hi-C matrix and returns a two lists of numpy arrays that contain the contact data at TAD start boundaries and TAD end boundaries
    """
    hicFile = pd.read_table(hicFileName, header = None)
    dim = hicFile.shape
    # Get start boundaries
    starts = tadCoords.loc[:,'start']
    # Get end boundaries
    ends = tadCoords.loc[:,'end']
    # Declare boundary sub-matrix containers
    startMatrices = [np.zeros(((subMatrSize * 2), (subMatrSize * 2))) for i in range(len(starts))]
    endMatrices = [np.zeros(((subMatrSize * 2), (subMatrSize * 2))) for i in range(len(starts))]
    # Get TAD boundary sub-matrices and store in container
    counter = 0
    for start in starts:
        startPaddedMin = int(start - subMatrSize)
        startPaddedMax = int(start + subMatrSize)
        if(startPaddedMin < 0):
            startPaddedMin = int(0)
            startPaddedMax = int(2 * subMatrSize)
        if(startPaddedMax > dim[0]):
            startPaddedMin = int(dim[0] - (2 * subMatrSize))
            startPaddedMax = int(dim[0])
        startMatrices[counter] = (hicFile.iloc[(startPaddedMin):(startPaddedMax), (startPaddedMin):(startPaddedMax)])
        counter = counter + 1
    counter = 0
    for end in ends:
        endPaddedMin = int(end - subMatrSize)
        endPaddedMax = int(end + subMatrSize)
        if(endPaddedMin < 0):
            endPaddedMin = int(0)
            endPaddedMax = int(2 * subMatrSize)
        if(endPaddedMax > dim[0]):
            endPaddedMin = int(dim[0] - (2 * subMatrSize))
            endPaddedMax = int(dim[0])
        endMatrices[counter] = (hicFile.iloc[(endPaddedMin):(endPaddedMax), (endPaddedMin):(endPaddedMax)])
        counter = counter + 1
    return(startMatrices, endMatrices)

def getBoundarySubMatrices_oneChr_boundariesOnly(hicFileName, tadCoords, subMatrSize):
    """
    Opens a Hi-C matrix and returns a one list of numpy arrays that contains the Hi-C contact data at TAD boundaries
    """
    hicFile = pd.read_table(hicFileName, header = None)
    dim = hicFile.shape
    # Get boundaries
    boundaries = tadCoords.loc[:,'boundary']
    # Declare boundary sub-matrix container
    boundaryMatrices = [np.zeros(((subMatrSize * 2), (subMatrSize * 2))) for i in range(len(boundaries))]
    
    # Get TAD boundary sub-matrices and store in container
    counter = 0
    for boundary in boundaries:
        boundaryPaddedMin = int(boundary - subMatrSize)
        boundaryPaddedMax = int(boundary + subMatrSize)
        if(boundaryPaddedMin < 0):
            boundaryPaddedMin = int(0)
            boundaryPaddedMax = int(2 * subMatrSize)
        if(boundaryPaddedMax > dim[0]):
            boundaryPaddedMin = int(dim[0] - (2 * subMatrSize))
            boundaryPaddedMax = int(dim[0])
        boundaryMatrices[counter] = (hicFile.iloc[(boundaryPaddedMin):(boundaryPaddedMax), (boundaryPaddedMin):(boundaryPaddedMax)])
        counter = counter + 1
    return(boundaryMatrices)

def getBoundarySubMatrices_multChr(hicFileNames, tadFileName, subMatrSize = 5, binSize = 40000):
    tadCoords = parseTadCoordinates(tadFileName)
    # Compute bin numbers from coordinates
    tadBinCoords = tadCoords
    tadBinCoords.loc[:,'start'] = tadCoords.loc[:,'start'] / binSize
    tadBinCoords.loc[:,'end'] = tadCoords.loc[:,'end'] / binSize
    
    for hicFileName in hicFileNames:
        # hicFileNames must end with .chr1, .chr2, ...
        # TODO: write test for filename end
        # Get the current chromosome ID from the filename extension
        currentChr = hicFileName.split('.chr')[1]
        print("Getting TAD boundary matrices for chromosome: " + currentChr)
        # Get the TAD coordinates for the current chromosome
        currentTadCoords = tadBinCoords[tadBinCoords['chr'] == 'chr' + currentChr]
        # Get the TAD boundary sub-matrices using the tad coordinates
        startMatrices,endMatrices = getBoundarySubMatrices_oneChr(hicFileName, currentTadCoords, subMatrSize)
        return(startMatrices,endMatrices)
    
def getBoundarySubMatrices_multChr_boundariesOnly(hicFileNames, tadFileName, subMatrSize = 5, binSize = 40000):
    """Similar as above but this method does not get TAD start and end sub-matrices but gets sub-matrices for unique TAD boundary locations only"""
    tadCoords = parseTadCoordinates_boundariesOnly(tadFileName)
    # Compute bin numbers from coordinates
    tadBinCoords = tadCoords
    tadBinCoords.loc[:,'boundary'] = tadCoords.loc[:,'boundary'] / binSize
    
    for hicFileName in hicFileNames:
        # hicFIleNames must end with .chr1, .chr2, ...
        # TODO: write test for filename end
        # Get the current chromosome ID from the filename extension
        currentChr = hicFileName.split('.chr')[1]
        print("Getting TAD boundary matrices for chromosome: " + currentChr)
        # Get the TAD coordinates for the current chromosome
        currentTadCoords = tadBinCoords[tadBinCoords['chr'] == 'chr' + currentChr]
        # Get the TAD boundary sub-matrices using the tad coordinates
        boundaryMatrices = getBoundarySubMatrices_oneChr_boundariesOnly(hicFileName, currentTadCoords, subMatrSize)
        return(boundaryMatrices)
    
        

if __name__ == "__main__":
    tad_file = "E:/MJT_hackathon/tads/hESC.domain_combined_boundariesonly.txt"
    matrix_files = ["E:\MJT_hackathon\cmatr\hES\eij\eij.chr20"]
    test = getBoundarySubMatrices_multChr_boundariesOnly(matrix_files, tad_file)
    print(test[0])