# Abstract
Neural networks for topologically associated domain (TAD) boundary detection

TADs are regions in the eukaryotic genome with an increased frequency of DNA-DNA interactions within their boundaries as opposed to across their boundaries. TAD boundaries have been shown to coincide with replication timing domains, are enriched for DNA binding proteins such as CTCF, Cohesin and Topoisomerase, and are believed to influence transcriptional activity by determining promoter-enhancer contacts. TADs were discovered by analysis of high throughput chromatin conformation capture sequencing (Hi-C) data and initial methods to determine the locations of TAD boundaries make use of the Chi-squared statistic based directionality index (DI), followed by a three-state hidden markov model. Subsequently, many alternative methods have been devised for detecting the locations of TAD boundaries. A major drawback for most of these algorithms is that they fo not incorporate replicate data in a meaningful way. Furthermore, many current TAD callers have poor running time as a result of the methodology, implementation or both.


Here, an implementation of a deep convolutional neural network (DCNN) is done for the detection of the genomic location of TAD boundaries. The implementation will be done using tensor flow and the DCNN will be trained on publicly available data. As a control, Hi-C data from cells under conditions where no TAD organization is present will be used. This approach should allow for straight-forward integration of replicate data. The problem is also parallelizable so an implementation using GPGPU (or other massively parallel processing) can be done, although this is likely beyond the scope of the 2018 hackathon.

# README
In this repository a neural network (in tflearn in python) is trained in order to predict TAD boundaries in Hi-C data. The trained network is then tested and sensitivity and specificity are calculated. FPs should be investigated since the possibility exists that the training set contains FNs.

Approach:

-Use Hi-C contact matrices and TAD partitionings from Dixon et al. as training data

-Train a deep convolutional neural network (tflearn) on known TAD-boundary locations in Hi-C contact matrices

-Use Hi-C contact matrices from mitotic cells and Cohesin-depleted cells as a control set (little or no TAD organization)

-Calculate sentitivity and specificity


Training matrices consist of subsets of Hi-C contact matrices that contain a TAD boundary as defined by Dixon et al. The amount of padding (number of bins to either side of the reported boundary) wil be randomly and independently (up- or downstream) assigned. This is done to prevent overfitting to boundaries that are centrally located.

Determning the number of convolution and max-pooling steps is going to be a matter of trial and error. 

As a starting point we will try: 

2 x convolution and max pooling

2 x convolution

1 x max pooling

2 fully connected NN layers


TAD classifier

The NN should be able to distinguish a contact (sub-)matrix containing a TAD boundary from a contact (sub-)matrix without a TAD boundary.
Per chromosome, we divide the contact map into overlapping regions of 3kb width with an overlap of 1kb.
If a region is classified as TAD-boundary-containing, we can perform another sliding window analysis within that region, to determine the boundary locations with greater accuracy.
