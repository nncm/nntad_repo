"""Convolutional network applied to Hi-C contact matrix data.
Links:
Contact maps: http://chromosome.sdsc.edu/mouse/hi-c/download.html
Convolutional NN intro (and code inspiration): https://medium.com/@ageitgey/machine-learning-is-fun-part-3-deep-learning-and-convolutional-neural-networks-f40359318721
"""
from __future__ import division, print_function, absolute_import

import tflearn
from tflearn.data_utils import shuffle, to_categorical
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
import numpy as np
import pandas as pd

import sys
sys.path.append('/home/participant/Desktop/Group_Share/nntad/michiel/nntad_repo/io')
from sys import exit

#import parse_cmatr to get TAD data
import parse_cmatr as pcm
# import build_control_data to get non-TAD data
import build_control_data as bcd

tad_domains = "/home/participant/Desktop/Group_Share/nntad/hicdata/mESC/HindIII_combined/total.HindIII.combined.domain"
matrix_file = "/home/participant/Desktop/Group_Share/nntad/hicdata/mES_bed/nij/nij.chr4"

tadCoords = pcm.returnTadTrainMatr(tad_domains)
tads = pcm.subset_matrix(matrix_file, tadCoords)

tads2 = []

j = tads.shape[0]
i = 0
while i < j:
	tmp = tads[i]
	tmp = pd.DataFrame(tmp)
	tmp = tmp.applymap(lambda x: [x])
	tads2.append(np.array(tmp.values.tolist()))
	i += 1

nottads = bcd.buildRandomNonTadData(j = 100, dims = [64, 64])
nottads_test = bcd.buildRandomNonTadData(j = 100, dims = [64, 64])

X = np.concatenate((tads2[int(np.floor(len(tads2)/2)):], nottads))
X_test = np.concatenate((tads2[:int(np.floor(len(tads2)/2))], nottads_test))

# B
Y = np.concatenate((
	np.array([[0, 1]]*len(tads2[int(np.floor(len(tads2)/2)):])),
	np.array([[1, 0]]*len(nottads))
	))

Y_test = np.concatenate((
	np.array([[0, 1]]*len(tads2[:int(np.floor(len(tads2)/2))])), 
	np.array([[1, 0]]*len(nottads_test))
	))

# Shuffle the data
X, Y = shuffle(X, Y)

# Real-time data preprocessing (normalization)
img_prep = ImagePreprocessing()
img_prep.add_featurewise_zero_center()
img_prep.add_featurewise_stdnorm()

# Define convolutional network architecture
dims = [64, 64]
network = input_data(shape = [None, dims[0], dims[1], 1],
	data_preprocessing = img_prep)
# Step 1: convolution (network, dimensions of image, pixel 'colour depth', activation function: rectified linear unit)
network = conv_2d(network, dims[0], 1, activation = 'relu')
# Step 2: max pooling
network = max_pool_2d(network, 2)
# Step 3: convolution with larger dimensions
network = conv_2d(network, dims[0]*2, 1, activation = 'relu')
# Step 4: max pooling
network = max_pool_2d(network, 2)
# Step 5: convolution
network = conv_2d(network, dims[0]*2, 1, activation = 'relu')
# Step 6: convolution
network = conv_2d(network, dims[0]*2, 1, activation = 'relu')
# Step 7: max pooling
network = max_pool_2d(network, 2)
# Step 8: fully connected NN
network = fully_connected(network, 512, activation = 'relu')
# Randomly remove some data to prevent overfitting
network = dropout(network, 0.5)
# Step 9: fully connected NN with two outputs (0: not TAD, 1: TAD)
network = fully_connected(network, 2, activation = 'softmax')
# Tell tflearn how we want to train the network
# network, adam: adaptive moment estimation, categorical_crossentropy: defaulf loss function, learning_rate: this layer optimizer's learning rate.
network = regression(network, 
	optimizer='adam',
	loss='categorical_crossentropy',
	learning_rate=0.001)

# Wrap the network in a model object
model = tflearn.DNN(network, tensorboard_verbose = 0, checkpoint_path = 'tad-classifier.tfl.ckpt')


# Train it the network. 10 training passes and monitor it as it goes.
model.fit(X, Y, 
	n_epoch = 100, 
	shuffle = True, 
	validation_set = (X_test, Y_test),
	show_metric = True, 
	batch_size = 96,
	snapshot_epoch = True,
	run_id = 'tad-classifier')

# Save model when training is complete to a file
model.save("tad-classifier.tfl")
print("Network trained and saved as tad-classifier.tfl!")

