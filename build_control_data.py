"""Generates non-TAD Hi-C contact map subsets
The NN needs a set of TAD data and a set of non-TAD data.
This script generates matrices that do not contain TADs.
There are three approaches:
-Random data from uniform distribution. This approach is simple but may not be very useful.
-Biological data where we are pretty confident that no TAD is present. This approach has the advantage of reflecting biology but there may be TADs present. This is because the Dixon TAD caller has low recall. I.e. there may be false negatives in the data. Therefore I cannot guarantee that Hi-C contact matrix data that is not encapsulated by TAD boundaries, truely does not contain TADs.
-Hi-C data from the mitotic phase. This approach also has the benefit that it reflects actual biology and takes advantage of the fact that there is no TAD structure in mitosis. Though it can be argued that interphase and mitotic phase DNA organisation are different to such a degree that this is an "unfair" control set
TODO: build a random data generator that samples from the negative binomial distribution
https://docs.scipy.org/doc/numpy-1.10.1/reference/generated/numpy.random.negative_binomial.html
"""
import numpy as np
import pandas as pd

def buildRandomNonTadData(j = 1000, dims = [64, 64], max = 50):
	"""Build random test data, sample from uniform distribution.
	Builds j-i matrices with dimensions dims[] with random integers
	The lower triangle (excluding the diagonal) contains only -1 values
	"""
	#nottads = np.zeros((j, dims[0], dims[1]))
	nottads = []

	i = 0
	while i < j:
		# Get a 2d array, of dimensions dims[0] by dims[1], with random integers between 0 and max
		#currentRandMatr = np.random.randint(0, max, size = (dims[0], dims[1]))
		currentRandMatr = np.random.uniform(0, max, size = (dims[0], dims[1]))
		# build a mask of the matrix and set all elements to True
		mask = np.ones([dims[0], dims[1]], dtype = 'bool')
		# set upper triangle of the mask to False (including the diagonal)
		mask[np.triu_indices(dims[0], 0)] = False
		# set the lower triangle elements in currentRandMatr to -1
		currentRandMatr[mask] = -1
		# cast to dataframe
		currentRandMatr = pd.DataFrame(currentRandMatr)
		# use applymap to turn each element into a list of length 1
		# if multiple chanels are required later on, add dimensions here
		# for instance, ChIP-Seq peaks could be added here or multiple Hi-C replicates could be merged here
		currentRandMatr = currentRandMatr.applymap(lambda x: [x])
		# cast to list and append to nottads
		nottads.append(np.array(currentRandMatr.values.tolist()))
		i += 1
	#TODO: cast to pd object and use Raquel's method to cast to lists
	return (np.array(nottads))

def buildNonTadBoundaryDelineatedData():
	return None

def buildMitoticTestData(fileName):
	
	return None

if __name__ == "__main__":
	a = buildRandomNonTadData(j = 2, dims = [5, 5])
	print a
	print type(a)
	print a.shape
	#k = (len(a))
	#while k > 0:
	#	print a[k-1]
	#	print "\n"
	#	k -= 1