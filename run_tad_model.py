#==============================================================================
# Run the model on TADs from chromosome 1 and new notTADs
#==============================================================================
from __future__ import division, print_function, absolute_import

import tflearn
from tflearn.data_utils import shuffle, to_categorical
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
import numpy as np
import pandas as pd

import sys
sys.path.append('/home/participant/Desktop/Group_Share/nntad/michiel/nntad_repo/io')
from sys import exit

#import parse_cmatr to get TAD data
import parse_cmatr as pcm
# import build_control_data to get non-TAD data
import build_control_data as bcd

matrix_file_chr9 = "/home/participant/Desktop/Group_Share/nntad/hicdata/mES_bed/nij/nij.chr3"
tad_domains = "/home/participant/Desktop/Group_Share/nntad/hicdata/mESC/HindIII_combined/total.HindIII.combined.domain"
tadCoords = pcm.returnTadTrainMatr(tad_domains, 3)
tads_chr9 = pcm.subset_matrix(matrix_file_chr9, tadCoords)

tads_chr9_2 = []
j = tads_chr9.shape[0]
i = 0
while i < j:
	tmp = tads_chr9[i]
	tmp = pd.DataFrame(tmp)
	tmp = tmp.applymap(lambda x: [x])
	tads_chr9_2.append(np.array(tmp.values.tolist()))
	i += 1

#tads_chr9_2 = np.array(tads_chr9_2)

nottads2 = bcd.buildRandomNonTadData(j = 100, dims = [64, 64])

# Same network definition as before
img_prep = ImagePreprocessing()
img_prep.add_featurewise_zero_center()
img_prep.add_featurewise_stdnorm()
dims = [64, 64]
network = input_data(shape = [None, dims[0], dims[1], 1],
	data_preprocessing = img_prep)
network = conv_2d(network, dims[0], 1, activation = 'relu')
network = max_pool_2d(network, 2)
network = conv_2d(network, dims[0]*2, 1, activation = 'relu')
network = max_pool_2d(network, 2)
network = conv_2d(network, dims[0]*2, 1, activation = 'relu')
network = conv_2d(network, dims[0]*2, 1, activation = 'relu')
network = max_pool_2d(network, 2)
network = fully_connected(network, 512, activation = 'relu')
network = dropout(network, 0.3)
network = fully_connected(network, 2, activation = 'softmax')
network = regression(network, 
	optimizer='adam',
	loss='categorical_crossentropy',
	learning_rate=0.001)
model = tflearn.DNN(network, tensorboard_verbose = 0, checkpoint_path = 'tad-classifier.tfl.ckpt')
model.load("tad-classifier.tfl.ckpt-20")

TP = 0
FP = 0
TN = 0
FN = 0

for currentTad in tads_chr9_2:
	prediction = model.predict([currentTad])
	if np.argmax(prediction[0]) == 1:
		TP += 1
	else:
		FN += 1

for currentNotTad in nottads2:
	prediction = model.predict([currentNotTad])
	if np.argmax(prediction[0]) == 1:
		FP += 1
	else:
		TN += 1

print("TP")
print(TP)
print("FP")
print(FP)
print("TN")
print(TN)
print("FN")
print(FN)
